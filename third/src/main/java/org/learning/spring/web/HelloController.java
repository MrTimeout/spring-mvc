package org.learning.spring.web;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.apache.commons.lang3.StringUtils.EMPTY;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.IntStream;
import org.learning.spring.model.Person;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public final class HelloController {

  private static final String BASE_PERSON_NAME = "person_name_";

  private final Map<Integer, Person> personMap;

  private final List<String> randomUUIDs;

  public HelloController() {
    this.personMap = IntStream.range(0, 10)
        .boxed()
        .collect(toMap(Function.identity(),
            i -> new Person(BASE_PERSON_NAME + i, BASE_PERSON_NAME + i)));
    this.randomUUIDs = IntStream.range(0, 10).boxed()
        .map(i -> UUID.randomUUID().toString())
        .collect(toList());
  }

  // Path: /roster/list[.*]
  // It will return the view of list.jsp
  @RequestMapping
  public void list(final Model model) {
    model.addAttribute("personMap", this.personMap);
  }

  // Path: /roster/member[.*]
  // It will return the view of member.jsp
  // DefaultRequestToViewNameTranslator is useful to change convention between request and view name in jsp el
  @RequestMapping
  public void member(@RequestParam("id") final Integer id, final Model model) {
    model.addAttribute(this.personMap.getOrDefault(id, new Person(EMPTY, EMPTY)));
  }

  @RequestMapping
  public void cheatsheet(final Model model) {
    model.addAllAttributes(new HashMap<String, Object>() {{
      put("personMap", personMap);
      put("randomUUIDs", randomUUIDs);
    }});
  }

  @RequestMapping
  public void another(final Model model) {
    model.addAttribute("personMap", this.personMap);
  }
}
