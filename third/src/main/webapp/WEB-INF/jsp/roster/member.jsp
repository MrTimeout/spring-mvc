<%-- Source project: sip03, branch: 01 (Maven Project) --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>Member: ${person}</title>
  </head>
  <body>
    <h1>Member: ${person}</h1>
    <p>
      <a href="list.do">Back</a>
    </p>
  </body>
</html>