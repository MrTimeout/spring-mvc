<%--
Characters to scape in JSP:
  &lt; is the lower than symbol
  &gt; is the higher than symbol
  &amp; is the ampersand
  &#039; is the single colon
  &#034; is the double colon
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.apache.commons.lang3.StringUtils, java.util.UUID" %>
<html>
  <head>
    <title>Cheatsheet</title>
  </head>
  <body>
    <h1>Cheatsheet</h1>
    <p>We can use &lt;%= new java.util.Date() %&gt; to execute some piece of code inline: <%= new java.util.Date() %></p>

    <%
      System.out.println("Here we are, printing in the console");
      out.println("<p> here we are, printing in the servlet");
      String uuid = java.util.UUID.randomUUID().toString();
    %>

    <%-- This is a scriptlet, so we can use also: <jsp:scriptlet>Code</jsp:scriptlet>--%>
    <%
      if ( org.apache.commons.lang3.StringUtils.contains(uuid, "0") ) {
        %>
        <p>The uuid <%= StringUtils.upperCase(uuid) %> contains a number 0</p>
        <%
      } else {
        %>
        <p>The uuid <%= StringUtils.upperCase(uuid) %> does not contain a number 0</p>
        <%
      }
    %>

    <%-- Directive --%>
    <%@ include file="another.jsp" %>

    <%-- Using tags to include another JSP file --%>
    <jsp:include page="another.jsp"/>

    <p>Before declaration of the function: 1 + 2 = <%= sum(1, 2) %> </p>
    <%-- Declaration --%>
    <%!
      Integer sum(final Integer a, final Integer b) {
        return a + b;
      }
    %>
    <p>After declaration of the function: 1 + 2 = <%= sum(1, 2) %> </p>

    <%-- Here we are using one of the keywords (pageContext and pageScope) to extract the attributes --%>
    <jsp:scriptlet>
      final String[] var = new String[]{ "Pedro", "Manolo", "Juan" };
      pageContext.setAttribute("names", var);
    </jsp:scriptlet>

    <%-- Here we are displaying the attribute stored in pageContext. We have to use pageScope to display these attributes stored before --%>
    <c:forEach var="name" items="${pageScope.names}">
      <p>(using c:out) This is the name of the person: <c:out value="${name}"/> </p>
      <p>(using dollar annotation) This is the name of the person: ${name}</p>
    </c:forEach>

    <%-- The mandatory fields are "var" and "items" --%>
    <c:forEach var="uuid" items="${randomUUIDs}">
      <p><c:out value="${uuid}"/></p>
    </c:forEach>
  </body>
</html>