<%-- This is a comment --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>Roster</title>
  </head>
  <body>
    <h1>Roster</h1>
    <ul>
      <c:forEach var="person" items="${personMap}"
      varStatus="status">
        <li>
          <a href="person.do?id=${status.index}">
            <c:out value="${person}"></c:out>
          </a>
        </li>
      </c:forEach>
    </ul>
  </body>
</html>